import React, { useContext } from "react";
import PropTypes from "prop-types";
import Theme from "./theme";
// just comparing the compiled output of these two style injections...
//import styles from "./styles.css";
import useStyles from "./styles.js";

// DONT FORGET TO EXPORT!!
export const ThemeContext = Theme;

const Nest1 = ({ ...props }) => {
  console.log(props);
  return <h1 className="badger">{props.children}</h1>;
};

const ExampleComponent = ({ ...props }) => {
  const theme = useContext(ThemeContext);
  console.log(theme);
  const classes = useStyles(theme);
  return (
    <div className={classes.test}>
      Example Component: {props.text}{" "}
      <Nest1 /*className={classes.nest}*/ foo="bar">stlyed?</Nest1>
    </div>
  );
};

export default ExampleComponent;
