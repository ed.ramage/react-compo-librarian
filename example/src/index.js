import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { ThemeContext } from "react-compo-librarian";
console.log("ssss", ThemeContext);

const themester = {
  backgroundColor: "yellow"
};

ReactDOM.render(
  <ThemeContext.Provider value={themester}>
    <App />
  </ThemeContext.Provider>,
  document.getElementById("root")
);
