import { makeStyles } from "@material-ui/styles";

const styles = theme => ({
  test: {
    border: "4px solid pink",
    "&> .badger": {
      backgroundColor: "green"
    },
    backgroundColor: theme => theme.backgroundColor
  },
  nest: {
    backgroundColor: "red"
  }
});

export default makeStyles(styles, { name: "yoyoyo" });
