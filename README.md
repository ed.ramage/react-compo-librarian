# react-compo-librarian

> crl learnin

[![NPM](https://img.shields.io/npm/v/react-compo-librarian.svg)](https://www.npmjs.com/package/react-compo-librarian) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-compo-librarian
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-compo-librarian'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [ed-ram](https://github.com/ed-ram)
