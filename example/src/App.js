import React, { useContext } from "react";
import ExampleComponent from "react-compo-librarian";

const App = () => {
  return (
    <div>
      <ExampleComponent text="Modern React component module" />
    </div>
  );
};

export default App;
